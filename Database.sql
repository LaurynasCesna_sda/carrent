CREATE DATABASE carrentproject2/*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;

CREATE TABLE carrentproject2.car (
  `ID` bigint NOT NULL AUTO_INCREMENT,
  `MAKE` varchar(100) DEFAULT NULL,
  `MODEL` varchar(100) DEFAULT NULL,
  `IS_AVAILABLE` tinyint(1) NOT NULL,
  `PHOTO_URL` varchar(2083) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO carrentproject2.car (MAKE,MODEL,IS_AVAILABLE,PHOTO_URL) VALUES 
('Toyota','Supra',1,'https://live.staticflickr.com/65535/50911772426_dcd6806495_k.jpg')
,('Ford','Puma',1,'https://live.staticflickr.com/65535/50911082633_1c06c1ec44_k.jpg')
,('Mitsubishi','Eclipse',1,'https://live.staticflickr.com/65535/50911082558_9725cf575e_k.jpg')
,('Mitsubishi','Lancer Evo 8',1,'https://live.staticflickr.com/65535/50911082478_cedb5a0a5c_k.jpg')
,('Toyota','Celica',1,'https://live.staticflickr.com/65535/50911082418_ae1adf9491_k.jpg')
,('Audi','RS4',1,'https://live.staticflickr.com/65535/50911772086_043cb41f68_k.jpg')
;

CREATE TABLE carrentproject2.user (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `has_rented` tinyint NOT NULL,
  `rented_cars_id` bigint DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_FK` (`rented_cars_id`),
  CONSTRAINT `user_FK` FOREIGN KEY (`rented_cars_id`) REFERENCES `car` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
