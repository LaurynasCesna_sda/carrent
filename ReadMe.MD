# Max Impression Car Rental Service 
## Final project for SDA Academy / Java from scratch course

## Table of Contents 
* [General info](#general-info)
* [Technologies used](#technologies-used)
* [Installation](#installation)
* [Features](#features)
* [Bugs](#bugs)
* [Status](#status)
* [Contact](#contact)



## General info
This project tries to simulate simple car rental service program and for a bit of fun factor added a little tuned car twist.
![Example screenshot](Screen.png)

## Technologies used 

* Spring-boot
* Spring-security 
* MySQL 
* Thymeleaf

## Installation
* Clone project and open with you preferred IDE (This project is built using InteliJ.Idea)<br/>

* Use included Database.sql file in your database software (Workbench , DBeaver etc. ) to create needed database 

* In  Core/src/main/resources/application.yml  change your database access information <br/>
![Example screenshot](File.png)<br/>
![Example screenshot](Data.png)

* run Core/src/main/java/com/LawRee/MainApp.java , 
then open your web browser and go to http://localhost:8080

Have Fun!

## Features
* Login and Register functions 
* Home page shows all cars , that "company" currently  operates
* User can browse and rent currently available cars ( When user rents a car , that car is flagged as rented and wont appear to other users )
* User can check currently rented car and return it 


## Bugs
* When user rents a car and goes back to UserHome page , it doesn't show "Check car" button , 
User then needs to Sign-out and Login again for it to appear
* Same with car return function - when user returns a car , user again needs to Sign-out and Login for button "Check available cars" to appear 

 ## Status 
 
 This project is still in development , currently working on fixing bugs listed above 
 
 ## Contact 
 
 You can contact me by email :
 djlawree@gmail.com
 
 On Facebook :
 Laurynas LawRee Čėsna
 
 
 
 





