package com.LawRee.repository;

import com.LawRee.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;


public interface UserRepo extends JpaRepository<User, Long>  {
    @Query("SELECT u FROM User u WHERE u.name = ?1")
    User findByUsername(String username);

    @Transactional
    @Modifying
//    @Query("UPDATE User u  SET u.hasRented = 1  WHERE u.id= :id ")
    @Query("UPDATE User u  SET u.hasRented = 1 ,u.rentedCarsId =:carId WHERE u.id= :id ")
    void setRented(@Param("id")Long id,@Param("carId") Long carId);
//    void setRented(@Param("id")Long id);
    @Transactional
    @Modifying
    @Query("UPDATE User u  SET u.hasRented = 0 ,u.rentedCarsId =null WHERE u.id= :id ")
    void setReturned(@Param("id")Long id);


}
