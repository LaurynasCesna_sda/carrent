package com.LawRee.repository;

import com.LawRee.model.Car;
import com.LawRee.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

public interface CarRepo  extends JpaRepository<Car, Long> {
    @Transactional
    @Modifying
    @Query("UPDATE Car c  SET c.isAvailable = 0 WHERE c.id= :carId ")
    void rentCar(@Param("carId")Long id);
 //   void rentCar(@Param("carId")Long id , @Param("userId") Long userId);

   // @Query("SELECT c FROM Car c WHERE c.rentedBy = :id")
   // Car findByRentedId(@Param("id") Long id);
    @Transactional
    @Modifying
    @Query("UPDATE Car c  SET c.isAvailable = 1 WHERE c.id= :carId ")
    void returnCar(@Param("carId") Long id );
}
