package com.LawRee.service;

import com.LawRee.model.Car;

public interface ICarService {

    void saveCar(Car carToAdd);
    void removeCar(Long id);
    void updateCar(Car carToUpdate);
    Car getCar(Long id);
}
