package com.LawRee.service;

import com.LawRee.model.Car;
import com.LawRee.model.User;
import com.LawRee.repository.CarRepo;
import com.LawRee.repository.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RentService {
    @Autowired
    private CarRepo carRepo;
    @Autowired
    private UserRepo userRepo;

    public void rentCar(Long carId){
        carRepo.rentCar(carId);
    }

    public void returnCar(Long carId){
        carRepo.returnCar(carId);
    }
}
