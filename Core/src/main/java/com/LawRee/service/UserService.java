package com.LawRee.service;

import com.LawRee.model.User;
import com.LawRee.repository.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    //=======fields=======\\
    @Autowired
    private UserRepo repo;




    //====methods====\\
    public void createUser(User user){
        repo.save(user);
    }

    public void deleteUser(Long id){
        repo.deleteById(id);
    }

    public User getUser(Long id){
       return repo.getOne(id);
    }

    public void setRented(Long userId,Long carId){
        repo.setRented(userId,carId);

    }

    public void setReturned(Long userId){
        repo.setReturned(userId);
    }

    public void refresh(){
    }




}
