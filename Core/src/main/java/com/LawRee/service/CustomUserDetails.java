package com.LawRee.service;

import com.LawRee.model.User;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
@Data
@Slf4j
public class CustomUserDetails implements UserDetails {

    private User user;

    public CustomUserDetails(User user ){
        this.user=user;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return null;
    }

    @Override
    public String getPassword() {
        return user.getPass();
    }

    @Override
    public String getUsername() {
        return user.getName();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    public String showUserName(){
        return user.getName();
    }
    public Long getUserId(){
        return user.getId();
    }
    public boolean showIsRented(){
        return user.isHasRented();
    }

    public Long showRentedCarsId(){
        Long rentedCarsId = user.getRentedCarsId();
        log.info("Does get rented cars id ? "+rentedCarsId);
        return rentedCarsId;
    }
}
