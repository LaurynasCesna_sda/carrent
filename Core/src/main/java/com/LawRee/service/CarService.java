package com.LawRee.service;

import com.LawRee.model.Car;
import com.LawRee.repository.CarRepo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class CarService implements ICarService {

    //============fields============\\
@Autowired
private CarRepo repo;


    //============implemented methods============\\
    @Override
    public void saveCar(Car carToAdd) {
            repo.save(carToAdd);
    }

    @Override
    public void removeCar(Long id) {
            repo.deleteById(id);
    }

    @Override
    public void updateCar(Car car) {

    }

    @Override
    public Car getCar(Long id) {
        return repo.getOne(id);
    }


    public List<Car> getCarList(){
        return  repo.findAll();
}
}
