package com.LawRee.controller;

import com.LawRee.model.Car;
import com.LawRee.model.User;
import com.LawRee.service.CarService;
import com.LawRee.service.RentService;
import com.LawRee.service.UserService;
import com.LawRee.util.Mappings;
import com.LawRee.util.ViewNames;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.Mapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
@Slf4j
@Controller
public class RentController {
    //==fields==\\
    @Autowired
    private final CarService carService;

    @Autowired
    private final RentService rentService;

    @Autowired
    private final UserService userService;

    //=====constructor=====\\
    public RentController(CarService carService, RentService rentService, UserService userService) {
        this.carService = carService;
        this.rentService = rentService;
        this.userService = userService;
    }

    //======methods=====\\

    @RequestMapping(Mappings.CAR_RENTED)
    public String carRented(HttpServletRequest request, Model model,
                            @RequestParam(value = "carId", defaultValue = "") Long carId,
                            @RequestParam(value = "userId", defaultValue = "") Long userId) {
        try {
            Car car = null;
            User user = null;
            if (carId != null) {
                car = carService.getCar(carId);
                user = userService.getUser(userId);
                log.info("Car fetched??" + car.getMake() + car.getModel());
                log.info("User is rented?? " + user.isHasRented());
                model.addAttribute("car",car);
            }
            if (car != null && !user.isHasRented()) {
                rentService.rentCar(car.getId());
                userService.setRented(user.getId(), car.getId());
                user.setHasRented(true);
                user.setRentedCarsId(car.getId());
                return ViewNames.CAR_RENTED;
            } else {
                System.out.println("You have already rented dipshit");
                return ViewNames.USER_HOME;
            }
        } catch (Exception e) {
            log.info("Exception : " + e);
            return ViewNames.INSPECT_CAR_VIEW_ERROR;
        }
    }

    @RequestMapping(Mappings.SHOW_RENTED_CAR)
    public String showRentedCar(HttpServletRequest request, Model model,
                                @RequestParam(value = "userId", defaultValue = "") Long userId){
        Car car = null;
        User user = userService.getUser(userId);
        car = carService.getCar(user.getRentedCarsId());
        model.addAttribute("car" ,car);
        model.addAttribute("user",user);
        return ViewNames.SHOW_RENTED_CAR;
    }

    @RequestMapping(Mappings.RETURN_CAR)
    public String returnCar(HttpServletRequest request,Model model,
                            @RequestParam(value = "userId",defaultValue = "") Long userId) {
        User user = userService.getUser(userId);
        Car car = carService.getCar(user.getRentedCarsId());
        if(car!=null ){
            rentService.returnCar(car.getId());
            userService.setReturned(user.getId());
            user.setRentedCarsId(null);
            user.setHasRented(false);
            return ViewNames.RETURN_CAR_VIEW;
        }

        return ViewNames.HOME;
    }


}