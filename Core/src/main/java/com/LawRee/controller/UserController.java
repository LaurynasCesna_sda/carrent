package com.LawRee.controller;

import com.LawRee.model.Car;
import com.LawRee.model.User;
import com.LawRee.service.CarService;
import com.LawRee.service.UserService;
import com.LawRee.util.Mappings;
import com.LawRee.util.ViewNames;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.annotation.SessionScope;

import java.util.List;


@Slf4j
@Controller
public class UserController {

    //======fields=====\\
    @Autowired
    private final UserService service;
    @Autowired
    private final CarService carService;

    //=====constructor???====\\

    public UserController(UserService service,CarService carService){
        this.service = service;
        this.carService=carService;
    }


    //=====methods=====\\
    @GetMapping(Mappings.REGISTER)
    public String register(Model model){
        User user=new User();
        model.addAttribute("user",user);
        return ViewNames.REGISTER;
    }

    @RequestMapping(value = Mappings.SAVE_USER , method = RequestMethod.POST)
    public String saveUser(@ModelAttribute("user") User user) {
        try {
            BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
            String encodedPassword = encoder.encode(user.getPass());
            user.setPass(encodedPassword);
            service.createUser(user);
            return ViewNames.HOME;
        }catch (Exception e){
            return ViewNames.EMAIL_ALREADY_IN_USE;
        }
    }


    @GetMapping(Mappings.USER_HOME)
    public String userHome( User user, Model model){

        return ViewNames.USER_HOME;
    }

}
