package com.LawRee.controller;

import com.LawRee.util.Mappings;
import com.LawRee.util.ViewNames;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class LoginController {

    @GetMapping(Mappings.LOGIN)
    public String login(){
        return ViewNames.LOGIN_VIEW;
    }
}
