package com.LawRee.controller;

import com.LawRee.model.Car;
import com.LawRee.model.User;
import com.LawRee.service.CarService;
import com.LawRee.service.RentService;
import com.LawRee.service.UserService;
import com.LawRee.util.Mappings;
import com.LawRee.util.ViewNames;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Slf4j
@Controller
public class CarController {
    //==fields==\\
    @Autowired
    private final CarService carService;

    @Autowired
    private final RentService rentService;

    @Autowired
    private final UserService userService;

    //==constructor==\\


    public CarController(CarService carService ,
                         RentService rentService,
                         UserService userService) {
        this.carService = carService;
        this.rentService = rentService;
        this.userService= userService;
    }

    //==request methods==\\
    @GetMapping(Mappings.ADD_CAR)
    public  String addCarPage(Model model){
        Car car = new Car();
        model.addAttribute("car" ,car);
        return ViewNames.ADD_CAR;
    }

    @RequestMapping(value = Mappings.SAVE , method = RequestMethod.POST)
    public String saveCar(@ModelAttribute("car") Car car){
        carService.saveCar(car);
        return ViewNames.HOME;
    }

    @GetMapping("")
    public String home(Model model) {
        List<Car> listCars = carService.getCarList();
        if (listCars != null) {
            //model.addAttribute("carService", carService);
            model.addAttribute("listCars", listCars);
            return ViewNames.HOME;
        }
        else return ViewNames.INSPECT_CAR_VIEW_ERROR;
    }

   @GetMapping(Mappings.CHECK_CARS)//("/checkCars")
    public String checkCarsPage(Model model){
       List<Car> listCars = carService.getCarList();
       if(listCars!=null) {
           //model.addAttribute("carService", carService);
           model.addAttribute("listCars", listCars);
           return ViewNames.INSPECT_CAR_VIEW;//(returns view named checkCars.html)
       } else {
           return ViewNames.INSPECT_CAR_VIEW_ERROR;//(returns view named error.html)
       }
    }

    @GetMapping(Mappings.AVAILABLE_CARS)
    public String availableCars(Model model){
        List<Car> listCars = carService.getCarList();
        model.addAttribute("listCars" , listCars);
        return  ViewNames.AVAILABLE_CARS;
    }




















/*
    //============fields============\\
    private final CarService carService;

    //============constructors============\\
    @Autowired
    public  CarStuffController (CarService carService){
        log.info("carService {}", carService);
        this.carService = carService;
    }

    //============model atributes===========\\
    @ModelAttribute
    public CarDataImpl carData(){
        log.info("CarStuffController-CarDataImpl carData");
        return carService.getCarData();
    }

    //============mappings?============\\

    @GetMapping(Mappings.CARS)
    public String cars(Model model){
        model.addAttribute("cars",carService.getCarData().getAllCars());
        return ViewNames.INSPECT_CAR_VIEW;
    } */
}