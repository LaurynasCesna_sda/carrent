package com.LawRee.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.awt.*;

@Entity
@Table
@Data
@EqualsAndHashCode(of="id")
public class Car {

    //============fields============\\
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="ID")
    private Long id;

    @Column(name="MAKE")
    private String make;

    @Column(name="MODEL")
    private String model;

    @Column(name="IS_AVAILABLE")
    private boolean isAvailable;

    @Column(name="PHOTO_URL")
    private String photo;

    //============constructor============\\

    public Car(String make, String model){
        this.make=make;
        this.model=model;
        this.isAvailable=true;
    }

    public Car(){
        this.isAvailable=true;
    }

}
