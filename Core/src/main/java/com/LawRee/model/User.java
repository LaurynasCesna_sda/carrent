package com.LawRee.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

@Data
@Entity
@Table(name = "user")
@EqualsAndHashCode(of="id")
public class User {

    //============fields============\\
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "NAME")
    private String name;
    @Column(name = "EMAIL")
    private String email;
    @Column(name="PASSWORD")
    private String pass;
    @Column(name="has_rented")
    private boolean hasRented;
    @Column(name="rented_cars_id")
    private Long rentedCarsId;

    //============constructor============\\
    public User(String name, String email, String pass){
        this.name = name;
        this.email = email;
        this.pass = pass;
        this.rentedCarsId=null;
        this.hasRented=false;
    }
    public User(){}

}
