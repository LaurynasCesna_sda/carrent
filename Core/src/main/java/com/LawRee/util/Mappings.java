package com.LawRee.util;
//reminder - these will appear on the end of url //

public final class Mappings {

    public static final String ADD_CAR="addCar";

    public static final String AVAILABLE_CARS="availableCars";

    public static final String CAR_RENTED="carRented";

    public static final String CHECK_CARS="checkCars";

    public static final String HOME="home";

    public static final String LOGIN="login";

    public static final String REGISTER="register";

    public static final String RETURN_CAR="returnCar";

    public static final String SAVE="save";

    public static final String SAVE_USER="saveUser";

    public static final String SHOW_RENTED_CAR="showRentedCar";

    public static final String USER_HOME="userHome";



    private Mappings(){}
}
