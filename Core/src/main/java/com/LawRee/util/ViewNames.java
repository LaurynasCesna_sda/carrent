package com.LawRee.util;
// for idiot coding this crap , aka " LawRee" : this shit will tell which view to open\\

public final class ViewNames {

    public static final String HOME="home";

    public static final String USER_HOME="userHome";

    public static final String INSPECT_CAR_VIEW = "checkCars";

    public static final String INSPECT_CAR_VIEW_ERROR="error";

    public static final String ADD_CAR="addCar";

    public static final String REGISTER="register";

    public static final String EMAIL_ALREADY_IN_USE="emailInUse";

    public static final String AVAILABLE_CARS="availableCars";

    public static final String CAR_RENTED="carRented";

    public static final String RETURN_CAR_VIEW= "returnCar";

    public static final String SHOW_RENTED_CAR="showRentedCar";

    public static final String LOGIN_VIEW="login";

    private ViewNames(){
    }
}
