$(document).ready(function() {
   $(window).on('scroll', function() {
    if($(window).scrollTop() < 1000) {
      $('.hero').css('background-size', 100 + parseInt($(window).scrollTop() / 5) + '%');
      $('.hero img').css('top', 15 + ($(window).scrollTop() * .2) + '%');
      $('.hero img').css('opacity', 1 - ($(window).scrollTop() * .003)); 
        $('.hero h1').css('top', 15 + ($(window).scrollTop() * .1) + '%');
      $('.hero h1').css('opacity', 1 - ($(window).scrollTop() * .005));
    }
     
     if($(window).scrollTop() >= $('.content-wrapper').offset().top - 600) {
       $('.nav-bg').removeClass('bg-hidden');
       $('.nav-bg').addClass('bg-visible');
     } else {
       $('.nav-bg').removeClass('bg-visible');
       $('.nav-bg').addClass('bg-hidden');
     }
  });
});